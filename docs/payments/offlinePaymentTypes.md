﻿# Альтернативные способы оплаты

Периодически случаются ситуации, когда участник не может или не хочет оплачивать стартовый взнос онлайн, но при этом готов оплатить свое участие каким-либо другим образом.

Каждый такой случай рассматривается организаторами в отдельном порядке, обычно участникам идут навстречу, но после того как оплата произошла (предположим, выполнен перевод на ваш телефон) вам необходимо отметить заявку этого участника как оплаченную.

Для этого найдите участника в общем списке участников (в группе "*Незавершенная регистрация**" внизу) и нажмите синюю кнопку справа:

![offlinePaymentTypes-step1.png](offlinePaymentTypes-step1.png "Пример экрана")

Откроется страница с информацией по данной заявке:

![offlinePaymentTypes-step2.png](offlinePaymentTypes-step2.png "Пример экрана")

Нажмите оранжевую кнопку "**Управление платежами**" в секции "Оплата", откроется следующая страница:

![offlinePaymentTypes-step3.png](offlinePaymentTypes-step3.png "Пример экрана")

Выберите подходящее значение в списке "**Альтернативные способы оплаты**" и нажмите кнопку "Сохранить".

Вы будете возвращены на список участников, где указанная заявка из секции "Незавершенная регистрация" переместится в список соответствующей дистанции. Если снова зайти в информацию по этому участнику, то вы увидите зеленый статус заявки `(1)`, указание того что оплата выполнена на телефон `(2)` и если открыть историю изменений `(3)` то увидите информацию о совершенном действии `(4)`.

![offlinePaymentTypes-step4.png](offlinePaymentTypes-step4.png "Пример экрана")