﻿Zareg.me: документация организатора соревнований
================================================

Данная документация предназначена для организаторов соревнований, открывающий регистрацию на сервисе Zareg.me_.

.. _Zareg.me: https://www.zareg.me

.. toctree::
   :maxdepth: 2
   :caption: Стартовые взносы

   payments/index
   payments/paymentMethod-YandexMoney
   payments/meetingPaymentMethod
   payments/meetingPaymentRules
   payments/offlinePaymentTypes