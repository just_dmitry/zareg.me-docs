﻿from recommonmark.parser import CommonMarkParser

source_parsers = {
    '.md': CommonMarkParser,
}

source_suffix = ['.rst', '.md']

master_doc = 'index'

project = 'Zareg.me'
copyright = u'2015, Дмитрий Попов'

language = 'ru'
